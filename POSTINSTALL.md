Config Documentation Link: https://owncast.online/docs/configuration/

Please change your stream key via the admin interface at /admin

The default admin login info (accessed via /admin) is:
Username: admin
Password: abc123 (THIS IS ALWAYS GOING TO BE YOUR STREAM KEY)
