FROM cloudron/base:3.0.0@sha256:455c70428723e3a823198c57472785437eb6eab082e79b3ff04ea584faf46e92

RUN mkdir -p /app/code /app/data
WORKDIR /app/code

# ffmpeg required
RUN apt-get -y update && \
    apt-get update && \
    apt -y install ffmpeg x264 x265 && \
    rm -rf /var/cache/apt /var/lib/apt/lists

ENV VERSION=0.0.11
RUN wget "https://github.com/owncast/owncast/releases/download/v${VERSION}/owncast-${VERSION}-linux-64bit.zip" -O /app/data/owncast.zip && \
unzip /app/data/owncast.zip -d /app/data

# copy start script
ADD start.sh /app/code/

CMD [ "/app/code/start.sh" ]
